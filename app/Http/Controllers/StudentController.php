<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Examinfo;
use App\Question;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $student = Student::get();
        //return $student;die();
        return view('student.show',compact('student'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function studentdetails(Request $request)
    {
        
        $student= new Student;
        $student->student_id = $request->student_id;
        $student->password = $request->password;
        $student->save();
        return "success";
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
      
        $student= new Student;
        $sIdForValidate=$request->input('student_id');
        $spasswordValidate=$request->input('password');
        $examCodeForValidate=$request->input('exam_code');
        $initialScore=0;
            
        $checker=Student::where('student_id','=',$sIdForValidate)->where('password','=',$spasswordValidate)->where('uniqueid','=',$examCodeForValidate)->count();
        
        if ($checker<0) {
            return "YOU ALREADY DONE THIS EXAM";
        }
        else{
            echo "hello";  
            /*$student = Student::update([
                //'student_id' => $request->input('student_id'),
                //'password' => $request->input('password'),
                'uniqueid' => $request->input('exam_code'),
                'score' =>$initialScore
                ]);*/
                $student->student_id = $request->student_id;
                $student->password = $request->password;
                $student->uniqueid = $request->uniqueid;
                //$student->score->$initialScore;
                $student->save();
    
                $id=$request->input('exam_code');
                $studentRealId=$request->input('student_id');
                $student_id=Student::where('student_id',$studentRealId)->value('id');
                $findcourse= Examinfo::where('uniqueid',$id)->value('id');
                $findtime= Examinfo::where('uniqueid',$id)->value('time');
                $course= Examinfo::where('uniqueid',$id)->value('Course');
                $questions=Question::where('quiz_id',$findcourse)->get();
                return view('answer.show')->with('questions', $questions)->with('student_id',$student_id)->with('course',$course)->with('time',$findtime);
        }
        /*if ($checker>0) {
            return "YOU ALREADY DONE THIS EXAM";
        }
        else{
            $student = Student::create([
            'student_id' => $request->input('student_id'),
            'password' => $request->input('password'),
            'uniqueid' => $request->input('exam_code'),
            'score' =>$initialScore
            ]);

            $id=$request->input('exam_code');
            $studentRealId=$request->input('student_id');
            $student_id=Student::where('student_id',$studentRealId)->value('id');
            $findcourse= Examinfo::where('uniqueid',$id)->value('id');
            $findtime= Examinfo::where('uniqueid',$id)->value('time');
            $course= Examinfo::where('uniqueid',$id)->value('Course');
            $questions=Question::where('quiz_id',$findcourse)->get();
            return view('answer.show')->with('questions', $questions)->with('student_id',$student_id)->with('course',$course)->with('time',$findtime);
        }*/
        

        //return $this->show($request->input('exam_code'));
    }
   /* public function updatedetails(Request $request)
    {
        
        $student= new Student;
        $sIdForValidate=$request->input('student_id');
        $spasswordValidate=$request->input('password');
        $examCodeForValidate=$request->input('exam_code');
        $initialScore=0;

        
     $checker=Student::where('student_id','=',$sIdForValidate)->where('password','=',$spasswordValidate)->count();
        
        if($checker < 0){
            return "already done this";
        }
        else{
            $student = Student::update([
                //'student_id' => $request->input('student_id'),
                //'password' => $request->input('password'),
                'uniqueid' => $request->input('exam_code'),
                'score' =>$initialScore
                ]);
               
                $id=$request->input('exam_code');
                $studentRealId=$request->input('student_id');
                $student_id=Student::where('student_id',$studentRealId)->value('id');
                $findcourse= Examinfo::where('uniqueid',$id)->value('id');
                $findtime= Examinfo::where('uniqueid',$id)->value('time');
                $course= Examinfo::where('uniqueid',$id)->value('Course');
                $questions=Question::where('quiz_id',$findcourse)->get();
                return view('answer.show')->with('questions', $questions)->with('student_id',$student_id)->with('course',$course)->with('time',$findtime);
        }
       
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('student.login', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        $student=  Student::find($id);
        $sIdForValidate=$request->input('student_id');
        $spasswordValidate=$request->input('password');
        $examCodeForValidate=$request->input('uniqueid');
        $initialScore=0;

        
     $checker=Student::where('password','=',$spasswordValidate)->where('student_id','=',$sIdForValidate)->count();
        
        if($checker > 0){
            return "incorrect password";
        }
        else{
            $id = Student::find($id);
           /* $student = Student::update([
                //'student_id' => $request->input('student_id'),
                //'password' => $request->input('password'),
                'uniqueid' => $request->input('exam_code'),
                'score' =>$initialScore,
                ]);*/
                /*$request->validate([
                    'password'=>'required',
                    'uniqueid'=>'required'
                ]);*/
                $student->uniqueid = $request->uniqueid;
                //$student->score->$initialScore;
                $student->save();
               
                $id=$request->input('uniqueid');
                $studentRealId=$request->input('student_id');
                $student_id=Student::where('student_id',$studentRealId)->value('id');
                $findcourse= Examinfo::where('uniqueid',$id)->value('id');
                $findtime= Examinfo::where('uniqueid',$id)->value('time');
                $course= Examinfo::where('uniqueid',$id)->value('Course');
                $questions=Question::where('quiz_id',$findcourse)->get();
                $countQuest = $questions->count();
                if($countQuest > 0){
                return view('answer.show')->with('questions', $questions)->with('student_id',$student_id)->with('course',$course)->with('time',$findtime);
            }
                else{
                    return "Please enter the valid exam code.";
                }
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
